[@bs.deriving jsConverter]
type t = [
  | `classic
  | `include_
  | [@bs.as "same-origin"] `sameOrigin
  | `omit
];