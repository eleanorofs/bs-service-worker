type _t('subtype, 'a, 'reason);
type t_like('subtype, 'a, 'reason) = Dom.event_like(_t('subtype, 'a, 'reason));
type t('a, 'reason) = t_like(Dom._baseClass, 'a, 'reason);


/* Sometimes I wonder if my commitment to 80-character lines has gone too
 * far.*/
[@bs.new]
external
make:
  (PromiseRejectionEventType.t, PromiseRejectionEventOptions.t('a, 'reason))
  => t('a, 'reason)
  = "PromiseRejectionEvent";

/* Properties */
[@bs.get] external promise: t('a, 'reason) => Js.Promise.t('a) = "promise";

[@bs.get] external reason: t('a, 'reason) => 'reason = "reason";

/* Event Handlers */

type handler('a, 'reason) = t('a, 'reason) => unit;

[@bs.get]
external
get_onrejectionhandled: t('a, 'reason)
  => handler('a, 'reason)
  = "onrejectionhandled";

[@bs.set]
external
set_onrejectionhandled: (t('a, 'reason), handler('a, 'reason))
  => unit
  = "onrejectionhandled";

[@bs.get]
external
get_onunhandledrejection: t('a, 'reason)
  => handler('a, 'reason)
  = "onunhandledrejection";

[@bs.set]
external
  set_onunhandledrejection: (t('a, 'reason), handler('a, 'reason))
  => unit
  = "onunhandledrejection";