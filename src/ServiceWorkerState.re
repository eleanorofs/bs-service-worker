[@bs.deriving jsConverter]
type t = [
  | `installing
  | `installed
  | `activating
  | `activated
  | `redundant
  ];
