type _t('a);
type t_like('a) = Dom.event_like(_t('a));
type t = t_like(Dom._baseClass);

/* properties */
[@bs.get] external data: t => 'data = "data";

[@bs.get] external origin: t => string = "origin";

[@bs.get] external lastEventId: t => string = "lastEventId";

//TODO property source. 

[@bs.get] external ports: t => array(MessagePort.t) = "ports";



