type t('a, 'reason) =
{ promise: Js.Promise.t('a)
, reason: 'reason
}