type t;

/* Methods */

[@bs.send] external enable: t => Js.Promise.t(unit) = "enable";

[@bs.send] external disable: t => Js.Promise.t(unit) = "disable";

