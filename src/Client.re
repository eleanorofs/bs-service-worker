type client;

type t = client;

module Impl = (T: { type t;}) => {
  [@bs.send]
  external postMessage: (client, 'a) => unit = "postMessage";

  /* TODO postMessage but with a transfer object. I'm not sure how I want that
  to translate, y'know? #opentopullrequest */

  [@bs.get] external id: client => string = "id";

  [@bs.deriving jsConverter]
  type type_ = [
    | `window
    | `worker
    | `sharedWorker
  ];

  [@bs.get] external type_: client => type_ = "type";

  [@bs.get] external url: client => string = "url";
};

include Impl({ type nonrec t = t; });